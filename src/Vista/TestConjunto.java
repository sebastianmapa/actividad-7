/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.seed.Conjunto;

/**
 *
 * @author SEBASTIAN
 */
public class TestConjunto {

    public static void main(String[] args) {

        Conjunto<Integer> conjunto = new Conjunto();
        conjunto.insertar(5);
        conjunto.insertar(8);
        conjunto.insertar(2);
        conjunto.insertar(7);
        conjunto.insertar(9);

        Conjunto<Integer> conjunto2 = new Conjunto();
        conjunto2.insertar(1);
        conjunto2.insertar(4);
        conjunto2.insertar(2);
        conjunto2.insertar(7);
        conjunto2.insertar(3);
        
 

        System.out.println(conjunto + "" + conjunto2);
        System.out.println(conjunto.getUnion(conjunto2));
        System.out.println(conjunto.getInterseccion(conjunto2));
        System.out.println(conjunto.getDiferencia(conjunto2));
        System.out.println(conjunto.getDiferenciaAsimetrica(conjunto2));
        System.out.println(conjunto.getPotencia());
    }
}
