/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util.seed;

/**
 * Wrapper (Envoltorio)
 *
 * @author docente
 * @param <T>
 */
public class Conjunto<T> implements IConjunto<T> {

    private ListaS<T> elementos;

    public Conjunto() {
        elementos = new ListaS<T>();
    }

    @Override
    public Conjunto<T> getUnion(Conjunto<T> c1) {
        Conjunto<T> union = new Conjunto<>();

        for (T elemento : this.elementos) {
            union.insertar(elemento);
        }

        for (T elemento : c1.elementos) {
            if (!union.contains(elemento)) {
                union.insertar(elemento);
            }
        }
        return union;
    }

    @Override
    public Conjunto<T> getInterseccion(Conjunto<T> c1) {
        Conjunto<T> interseccion = new Conjunto<>();
        for (T elemento : this.elementos) {
            if (c1.contains(elemento) && !interseccion.contains(elemento)) {
                interseccion.insertar(elemento);
            }
        }
        return interseccion;
    }

    @Override
    public Conjunto<T> getDiferencia(Conjunto<T> c1) {
        Conjunto<T> diferencia = new Conjunto<>();

        for (T elemento : this.elementos) {
            if (!c1.contains(elemento)) {
                diferencia.insertar(elemento);
            }
        }
        return diferencia;
    }

    @Override
    public Conjunto<T> getDiferenciaAsimetrica(Conjunto<T> c1) {
        Conjunto<T> CAsimetrica = new Conjunto<>();

        for (T elemento : this.elementos) {
            if (!c1.contains(elemento)) {
                CAsimetrica.insertar(elemento);
            }
        }

        for (T elemento : c1.elementos) {
            if (!this.contains(elemento)) {
                CAsimetrica.insertar(elemento);
            }
        }
        return CAsimetrica;
    }

    @Override
    public T get(int i) {
        return elementos.get(i);
    }

    @Override
    public void set(int i, T info) {
        elementos.set(i, info);
    }

    @Override
    public void insertar(T info) {
        elementos.insertarInicio(info);
    }

    public int size() {
        return this.elementos.getSize();
    }

    @Override
    public Conjunto<Conjunto<T>> getPotencia() {
        Conjunto<Conjunto<T>> potencia = new Conjunto<>();
        int n = elementos.getSize();

        int total = 1 << n; // 2^n

        for (int i = 0; i < total; i++) {
            Conjunto<T> subconjunto = new Conjunto<>();
            for (int j = 0; j < n; j++) {
                if ((i & (1 << j)) != 0) {
                    subconjunto.insertar(elementos.get(j));
                }
            }
            potencia.insertar(subconjunto);
        }
        return potencia;
    }

    @Override
    public String toString() {
        return elementos.toString();
    }

    public boolean contains(T info) {
        return elementos.contains(info);
    }

}
