/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util.seed;

import java.util.Iterator;

/**
 *
 * @author DOCENTE
 */
public class ListaCD<T> implements Iterable<T> {

    private NodoD<T> cabeza;
    private int size = 0;

    public int getSize() {
        return size;
    }

    public ListaCD() {

        this.cabeza = new NodoD(null, null, null);
        this.cabeza.setAnt(cabeza);
        this.cabeza.setSig(cabeza);
    }

    public void insertarInicio(T info) {
        NodoD<T> nuevo = new NodoD(info, this.cabeza.getSig(), this.cabeza);
        this.cabeza.setSig(nuevo);
        nuevo.getSig().setAnt(nuevo);
        this.size++;
    }

    public void insertarFin(T info) {
        NodoD<T> nuevo = new NodoD(info, this.cabeza, this.cabeza.getAnt());
        this.cabeza.setAnt(nuevo);
        nuevo.getAnt().setSig(nuevo);
        this.size++;
    }

    @Override
    public String toString() {
        String msg = "";
        for (NodoD<T> x = this.cabeza.getSig(); x != this.cabeza; x = x.getSig()) {
            msg += x.getInfo() + "<->";
        }
        return "cab<->" + msg + "cab";
    }

    public boolean isVacia() {
        //this.size==0
        //this.cabeza==this.cabeza.getAnt()
        return this.cabeza == this.cabeza.getSig();
    }

    public T remove(int i) {
        validar(i);
        NodoD<T> borrar = this.getPos(i);
        borrar.getAnt().setSig(borrar.getSig());
        borrar.getSig().setAnt(borrar.getAnt());
        this.size--;
        borrar.setAnt(null);
        borrar.setSig(null);
        return borrar.getInfo();
    }

    private void validar(int i) {
        if (this.isVacia() || i < 0 || i >= this.size) {
            throw new RuntimeException("No es válida la posición");
        }
    }

    private NodoD<T> getPos(int posicion) {
        this.validar(posicion);
        NodoD<T> x = this.cabeza.getSig();
        // a=3, b=a; b=3
        while (posicion-- > 0) {

            x = x.getSig();
            //pos=pos-1 : --> first class OP :(
        }
        return x;
    }

    public T get(int i) {
        try {
            this.validar(i);
            NodoD<T> pos = this.getPos(i);
            return pos.getInfo();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    public void set(int i, T info) {
        try {
            this.validar(i);
            NodoD<T> pos = this.getPos(i);
            pos.setInfo(info);
        } catch (Exception e) {
            System.err.println(e.getMessage());

        }
    }

    @Override
    public Iterator<T> iterator() {
        return new IteratorLCD(this.cabeza);
    }

    //interseccion de conjuntos
    public ListaCD<T> getInterseccion(ListaCD<T> l1) {
        if (this.isVacia() || l1.isVacia()) {
            throw new RuntimeException("Al menos una de las listas esta vacia");
        }
        ListaCD<T> result = new ListaCD();
        ListaCD<T> mayor = this;
        ListaCD<T> menor = l1;
        if (this.size < l1.size) {
            mayor = l1;
            menor = this;
        }
        Iterator<T> it = menor.iterator();
        while (it.hasNext()) {
            T info = it.next();
            if (mayor.contains(info)) {
                result.insertarFin(info);
            }
        }
        return result;
    }

    public boolean contains(T info) {
        Iterator<T> it = this.iterator();
        while (it.hasNext()) {
            if (it.next().equals(info)) {
                return true;
            }
        }
        return false;
    }

    public void concat(ListaCD<T> l1, int i) {
        validar(i);

        NodoD<T> nodoActual = this.getPos(i);
        NodoD<T> nodoSigActual = nodoActual.getSig();
        NodoD<T> nodoUltL1 = l1.cabeza.getAnt();

        nodoActual.setSig(l1.cabeza.getSig());
        nodoSigActual.setAnt(nodoUltL1);
        l1.cabeza.setAnt(nodoActual);
        nodoUltL1.setSig(nodoSigActual);

        this.size += l1.size;
        l1.cabeza = l1.cabeza.getSig();
        l1.size = 0;
    }

    /**
     * Método que permite operar con 2 listas ordenadas ascendentemente y
     * retorna la intersección en otra lista ordenada
     *
     * @param l1 Una lista ordenada
     * @return Una lista tipo T
     */
    public ListaCD<T> getInterseccionOrdenado(ListaCD<T> l1) {
        if (this.isVacia() || l1.isVacia()) {
            throw new RuntimeException("Al menos una de las listas está vacía.");
        }

        ListaCD<T> interseccionOrdenado = new ListaCD<>();
        NodoD<T> nodoActual = this.cabeza.getSig();
        NodoD<T> nodoL1 = l1.cabeza.getSig();

        while (nodoActual != this.cabeza && nodoL1 != l1.cabeza) {
            int comparacion = ((Comparable<T>) nodoActual.getInfo()).compareTo(nodoL1.getInfo());

            if (comparacion == 0) {
                interseccionOrdenado.insertarFin(nodoActual.getInfo());
                nodoActual = nodoActual.getSig();
                nodoL1 = nodoL1.getSig();
            } else if (comparacion < 0) {
                nodoActual = nodoActual.getSig();
            } else {
                nodoL1 = nodoL1.getSig();
            }
        }
        return interseccionOrdenado;
    }

    /**
     * Obtiene verdadero o falso si un elemento existe en una lista ordenada
     * ascendentemente No usar iterador
     *
     * @param elemento
     * @return
     */
    public boolean contains_sort(T elemento) {
        NodoD<T> nodoAux = this.cabeza.getSig();

        while (nodoAux != this.cabeza) {
            int comparacion = ((Comparable<T>) nodoAux.getInfo()).compareTo(elemento);

            if (comparacion == 0) {
                return true;
            } else if (comparacion > 0) {
                return false;
            }
            nodoAux = nodoAux.getSig();
        }
        return false;
    }
}
